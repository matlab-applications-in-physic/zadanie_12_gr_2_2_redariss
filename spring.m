%Author: Radoslaw Paluch
%Institute of Physics
%Technical Physics

clear

r=0.002;% wire radius [m]
N=2:1:50; %number of coils 
R=2/39.327;% coil radius [m]
G=77.2e9; %shear modulus [Pa] 
M=0.01; %mass of object [kg]
tau=2; %time coefficient [s]
%calculating beta
beta=1/2/tau;
%input frequency
omega=50:0.1:400;
%acceleration of extortion force
alfa0=9.81;
for j=1:size(N,2)
  %calculating spring coefficent[N/m]
  k=G*r^4/(4*N(j)*R^3);
  %calculating natural frequency
  omega0=sqrt(k/M);
  
  for i=1:size(omega,2)
    A(i,j)=alfa0/sqrt((omega0^2-omega(i)^2)^2+4*beta^2*omega(i)^2);
  end
  %finding maksimum amplitude to determine resonance frequency
  [maksA(j),index]=max(A(:,j));
  resonanceFrequency(j)=omega(index)/(2*pi);%dividing by 2pi to get Hz
end
%plotting data
subplot(2,1,1);
plot(omega/(2*pi),A);
title('Resonance curves for diffrent number of coils')
xlim([10,60])
xlabel('extortion force frequency[Hz]');
ylabel('amplitude[m]');
grid on; 

subplot(2,1,2);
scatter(N,maksA,'filled');
title('Resonance amplitude for diffrent number of coils')
xlabel('number of coils');
ylabel('resonance amplitude[m]');
grid on;
saveas(gcf,'resonance.pdf');

%saving data to file
fwd=fopen('resonance_analysis_results.dat','w');
fprintf(fwd,'r=%.3f[m];R=%.3f[m];M=%.2f[kg];tau=%.1f[s];alfa0=%.2f[m/s^2];G =%.2d[Pa]\n\n',r,R,M,tau,alfa0,G);

for i = 1:size(N,2)
    
	fprintf(fwd,'N=%.0f;A_res=%.3f[m];f_res=%.3f[Hz]\n',N(i),maksA(i),resonanceFrequency(i));

end

fclose(fwd);
